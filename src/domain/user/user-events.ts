import { DomainEvent } from "../DomainEvent";
import { User } from "./User";



export class UserRegistered extends DomainEvent<User> {
    constructor(public uuid:string, public email:string, public password:string){
        super();
    }
}

export class UserDeleted extends DomainEvent<User> {
    constructor(public uuid:string) {
        super();
    }
}

export class UserLogged extends DomainEvent<User> {
    constructor(public uuid:string){
        super();
    }
}

export class UserFailedLogin extends DomainEvent<User> {
    constructor(public uuid:string){
        super();
    }
}