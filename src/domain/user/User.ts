import { DomainEvent } from "../DomainEvent";

import {v4} from 'uuid';
import { UserDeleted, UserLogged, UserRegistered, UserFailedLogin } from "./user-events";



export class User {
    private uuid:string;
    private deleted = false;
    private password:string;
    private failedLogin = 0;

    constructor(domainEvents:DomainEvent<User>[]) {
        for (const event of domainEvents) {
            this.apply(event);
        }

    }


    private apply(event: DomainEvent<User>) {
        if (event instanceof UserRegistered) {
            this.uuid = event.uuid;
            this.password = event.password;

        }
        if (event instanceof UserDeleted) {
            this.deleted = true;
        }
        if(event instanceof UserFailedLogin) {
            this.failedLogin++;
        }
        //TODO: remettre le compteur à zéro si ya un login réussi
        
    }

    static register(email:string, password:string, publishEvent:Function) {
        if(!validateEmail(email)) return;
        if(password.length < 4) return;

        const uuid = v4();
        const event = new UserRegistered(uuid, email, password);
        
        publishEvent(event);
        
        return uuid;
    }

    deleteAccount(publishEvent:Function) {
        if(this.deleted) return;

        const event = new UserDeleted(this.uuid);
        this.apply(event);
        publishEvent(event);
    }

    login(password:string, publishEvent:Function) {
        if(this.deleted || this.failedLogin >= 3) return;

        let event:UserFailedLogin|UserLogged;
        if(this.password !== password) {
            event = new UserFailedLogin(this.uuid);
        } else {
            event = new UserLogged(this.uuid);
        }

        this.apply(event);
        publishEvent(event);
    }
}


function validateEmail(email):boolean {
    //Algo qui vérifie si l'email est valide
    return true;
}