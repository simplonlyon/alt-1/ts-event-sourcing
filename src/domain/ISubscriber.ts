import { DomainEvent } from "./DomainEvent";

export interface ISubscriber {
    handleEvent(event:DomainEvent<any>):void;
}