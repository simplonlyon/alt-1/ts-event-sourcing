 import { DomainEvent } from "./domain/DomainEvent";
 import { User } from "./domain/user/User";
import { UserRegistered } from "./domain/user/user-events";

import EventStore from "./infrastructure/EventStore";
import { Publisher } from "./infrastructure/Publisher";

// const user = new User(history);

// user.login('1234', publishEvent);

// console.log(history);

let publisher = new Publisher()
let store = new EventStore();
publisher.subscribe(store);

store.find("test").then(re => {
    let user = new User(re)
    user.login("1234", publisher.publishEvent.bind(publisher))
  
})