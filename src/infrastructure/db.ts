import {MongoClient} from "mongodb";

const URI = "mongodb://localhost:27017"
let client = new MongoClient(URI)

export async function getConnection() {
    let connection = await client.connect();
    const db = await connection.db("ts-event-sourcing");
    return db
}

