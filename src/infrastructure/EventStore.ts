import { DomainEvent } from "../domain/DomainEvent";
import { IEventStore } from "../domain/IEventStore";
import {getConnection} from './db';
import * as events from '../domain/user/user-events';
import { ISubscriber } from "../domain/ISubscriber";


export default class EventStore implements IEventStore, ISubscriber{
    /**
     * C'est super.
     * @author Skeller
     */
    handleEvent(event: DomainEvent<any>): void {
        this.save(event);
    }

    async save(event: DomainEvent<any>): Promise<void> {
        let con = await getConnection();
        con.collection("event").insertOne({...event, type: event.constructor.name});
    }


    async find(uuid: string): Promise<DomainEvent<any>[]> {
        let con = await getConnection();
        let resultArray = await con.collection("event").find({uuid}).toArray();

        let eventList = [];
        for (const result of resultArray) {
            let instance = new events[result.type]();
            Object.assign(instance, result);
            eventList.push(instance);
        }
        return eventList;
    }
}

